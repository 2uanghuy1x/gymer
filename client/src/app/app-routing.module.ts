import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from './routes/navbar/navbar.component';
import { PageNotFoundComponent } from './routes/page-not-found/page-not-found.component';
import { MembershipComponent } from './routes/membership/membership.component';
import { StatisticsComponent } from './routes/statistics/statistics.component';
import { ManagementComponent } from './routes/management/management.component';
import { SupportComponent } from './routes/support/support.component';

const routes: Routes = [
  { path: '', redirectTo: '/cinema', pathMatch: 'full' },
  {
    path: '', component: NavbarComponent,
    children: [
      { path: 'membership', component: MembershipComponent },
      { path: 'statistics', component: StatisticsComponent },
      { path: 'management', component: ManagementComponent },
      { path: 'support', component: SupportComponent },
      { path: '**', pathMatch: 'full', component: PageNotFoundComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
