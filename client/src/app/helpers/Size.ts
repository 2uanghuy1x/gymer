export enum Size
{
    S = 1,
    M = 2,
    L = 3,
}
export enum Type
{
    BotDinhDuong = 1,
    PhuKien = 2,
}
