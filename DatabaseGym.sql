﻿--drop database Gym
create database Gym
go
use Gym
go

--Bảng loại rank khách hàng
create table Rank(
	Id int identity primary key,
	FromPoint int,
	ToPoint int,
	Rank varchar(10),
)
go
-- goi tap 
create table EpisodePack(
	Id uniqueidentifier primary key default newid(),
	CreationTime datetime not null,
	CreatorUserId uniqueidentifier not null,
	LastModificationTime datetime null,
	LastModifierUserId uniqueidentifier null ,
	IsDeleted bit not null,
	DeleteTime datetime null,
	DeleterUserId uniqueidentifier null,
	EpisodePackName nvarchar(50) not null,
	EpisodePackMonth int not null,
	Price int not null,
)
go
create table Staff(
	Id uniqueidentifier primary key default newid(),
	CreationTime datetime not null,
	CreatorUserId uniqueidentifier not null,
	LastModificationTime datetime null,
	LastModifierUserId uniqueidentifier null ,
	IsDeleted bit not null,
	DeleteTime datetime null,
	DeleterUserId uniqueidentifier null,
	Name nvarchar(50) not null,
	DoB datetime not null,
	Sex bit not null,
	Type tinyint not null,
)
go
--Đồ ăn
create table AccessoryAndFood(
	Id uniqueidentifier primary key default newid(),
	CreationTime datetime not null,
	CreatorUserId uniqueidentifier not null,
	LastModificationTime datetime null,
	LastModifierUserId uniqueidentifier null ,
	IsDeleted bit not null,
	DeleteTime datetime null,
	DeleterUserId uniqueidentifier null,
	Name nvarchar(50) not null,
	Type tinyint not null,
	Size nvarchar(50) not null,
	Price int not null,
	--Trademark nvarchar(50) not null,
	--CinemaId uniqueidentifier not null,
	--constraint FK_FoodCinema foreign key (CinemaId) references Cinema(Id),
	--constraint FK_FoodSize foreign key (Size) references Size(Id),
)
go


--Bảng tài khoản để đăng nhập vào hệ thống
create table Account(
	Id uniqueidentifier primary key default newid(),
	CreationTime datetime not null,
	LastModificationTime datetime null,
	LastModifierUserId uniqueidentifier null,
	IsDeleted bit not null,
	DeleteTime datetime null,
	DeleterUserId uniqueidentifier null,
	Email nvarchar(50) not null,
	Password nvarchar(30) not null,
	Role int not null,
	Name nvarchar(50) not null,
	IdentityCard varchar(12) null,
	DoB datetime null,
	Address nvarchar(max) null,
	Phone varchar(10) null,
	Point int null,
	RankId int null,
	--constraint FK_AccountRank foreign key (RankId) references Rank(Id),
)
go

--Bảng mã khuyến mãi
create table Promotion(
	Id uniqueidentifier primary key default newid(),
	CreationTime datetime not null,
	CreatorUserId uniqueidentifier not null,
	LastModificationTime datetime null,
	LastModifierUserId uniqueidentifier null ,
	IsDeleted bit not null,
	DeleteTime datetime null,
	DeleterUserId uniqueidentifier null,
	Code nvarchar(50) not null,
	Price int not null,
	StartDate datetime not null,
	EndDate datetime not null,
)
go
--hóa đơn
create table Bill(
	Id uniqueidentifier primary key default newid(),
	CreationTime datetime not null,
	CreatorUserId uniqueidentifier not null,
	LastModificationTime datetime null,
	LastModifierUserId uniqueidentifier null ,
	IsDeleted bit not null,
	DeleteTime datetime null,
	DeleterUserId uniqueidentifier null,
	AccountId uniqueidentifier not null,
	Cost int,
	constraint FK_BillAccount foreign key (AccountId) references Account(Id),
)
go

--view danh sách các mã khuyến mãi
create view GetAllPromotion
as
	select Id, Code, Price, StartDate, EndDate from Promotion where IsDeleted <> 1
go

create proc SearchPromotion
@Id uniqueidentifier, @Price int, @StartDate datetime,@EndDate datetime
as
	select * from Promotion m where IsDeleted <> 1
	and (isnull(@Id, '00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' or m.Id  in (SELECT * FROM STRING_SPLIT(cast(@Id as varchar(Max)),',')))
    and (isnull(@Price, '') = '' or m.Price = @Price )
    and (isnull(@StartDate, '') = '' or cast(m.StartDate as date) = @StartDate)
    and (isnull(@EndDate, '') = '' or cast(m.EndDate as date ) = @EndDate)
      
go
--proc getall food in cinema
create or alter proc GetAllAccessoryAndFood
@CinemaId uniqueidentifier, @Name nvarchar(50), @Size int
as
	select * from AccessoryAndFood f 
	where f.IsDeleted <> 1
	and (isnull(@Name, '') = '' or upper(f.Name) like '%' + upper(@Name) + '%')
	and (isnull(@Size, '') = '' or upper(f.Size) like '%' + upper(@Size) + '%')
    option (recompile)
go
create proc SearchAccessoryAndFood
 @Name nvarchar(50), @Size int, @Price int ,@Type tinyint
as
	select * from AccessoryAndFood f where f.IsDeleted <> 1
	and (isnull(@Name, '') = '' or upper(f.Name) like '%' + upper(@Name) + '%')
	and (isnull(@Size, '') = '' or upper(f.Size) like '%' + upper(@Size) + '%')
	and (isnull(@Price, '') = '' or f.Price like '%' + @Price + '%')
	and (isnull(@Type, '') = '' or f.Type like '%' + @Type + '%')
    option (recompile)
go
--SUBSTRING(string, start, length)
--proc view Account nếu có tìm kiếm sẽ tìm theo yêu cầu không thì sẽ hiện full
create proc GetViewAccount
@Name nvarchar(50), @Email nvarchar(50), @IdentityCard nvarchar(12), @DoB datetime, @Address nvarchar(max), @Phone nvarchar(10)
as
	select * from Account a where a.IsDeleted <> 1
		and (isnull(@Name, '') = '' or upper(a.Name) like '%' + upper(@Name) + '%')
        and (isnull(@Email, '') = '' or upper(a.Email) like '%' + upper(@Email) + '%')
        and (isnull(@IdentityCard, '') = '' or upper(a.IdentityCard) like '%' + upper(@IdentityCard) + '%')
        and (isnull(@DoB, '') = '' or upper(a.DoB) like '%' + upper(@DoB) + '%')
        and (isnull(@Address, '') = '' or upper(a.Address) like '%' + upper(@Address) + '%')
        and (isnull(@Phone, '') = '' or upper(a.Phone) like '%' + upper(@Phone) + '%')
        option (recompile)
go
create proc SearchAccount
@Id nvarchar(Max),@TimeStart datetime, @TimeEnd datetime, @FromPoint int, @ToPoint int
as
	select * from Account a where a.IsDeleted <> 1
		and (isnull(@Id, '00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' or a.Id  in (SELECT * FROM STRING_SPLIT(cast(@Id as varchar(Max)),',')))
        and (isnull(@TimeStart, '') = '' or cast (a.DoB  as date) between @TimeStart and @TimeEnd)
		and (isnull(@FromPoint, '') = '' or a.Point between @FromPoint and @ToPoint)
        option (recompile)
go
--insert into Account(CreationTime, IsDeleted, Email, Password, Role, Name, IdentityCard, DoB, Address, Phone, Point) values (getdate(), 0, 'admin@gmail.com', '123456', 2, N'ad văn min', '001122334455', '2001-01-01', N'Hà Nội', '0123456789', 0)
--declare @existEmail int  = (select  count(*) from Account where Email = 'admin@gmail.com')
--proc add account
create proc CreateAccount
@Email nvarchar(50), @Password nvarchar(30), @Role int, @Name nvarchar(50), @IdentityCard varchar(12), @DoB datetime, @Address nvarchar(max), @Phone varchar(10)
as
	declare @existEmail int   = (select count(*) from Account where Email = @Email)
	declare @existId int  = (select count(*) from Account where IdentityCard = @IdentityCard)
	if (@existEmail <> 1 and @existId <> 1)
	begin
		insert into Account(CreationTime, IsDeleted, Email, Password, Role, Name, IdentityCard, DoB, Address, Phone, Point) values (getdate(), 0, @Email, @Password, @Role, @Name, @IdentityCard, @DoB, @Address, @Phone, 0)
	end
go
--proc update account
create or alter proc UpdateAccount
@LastModifierUserId uniqueidentifier, @Id uniqueidentifier, @Email nvarchar(50), @Password nvarchar(30), @Role int, @Name nvarchar(50), @IdentityCard varchar(12), @DoB datetime, @Address nvarchar(max), @Phone varchar(10), @Point int
as
	declare @existEmail int  = (select count(*) from Account where Email = @Email and Id != @Id)
	declare @existId int = (select count(*) from Account where IdentityCard = @IdentityCard and Id != @Id )
	if (@existEmail <> 1 and @existId <> 1)
	begin
		update Account set LastModificationTime = getdate(), LastModifierUserId = @LastModifierUserId, Email = @Email, Password = @Password, Name = @Name, IdentityCard = @IdentityCard, DoB = @DoB, Address = @Address, Phone = @Phone, Point = @Point where Id = @Id
	end
go
--proc add promotion
create proc CreatePromotion
@CreatorUserId uniqueidentifier, @Code nvarchar(50), @Discount int, @StartDate datetime, @EndDate datetime
as
	insert into Promotion(CreationTime, CreatorUserId, IsDeleted, Code, Price, StartDate, EndDate) values (getdate(), @CreatorUserId, 0, @Code, @Discount, @StartDate, @EndDate)
go
--proc update promotion
create proc UpdatePromotion
@LastModifierUserId uniqueidentifier, @Id uniqueidentifier, @Code nvarchar(50), @Discount int, @StartDate datetime, @EndDate datetime
as
	update Promotion set LastModificationTime = getdate(), LastModifierUserId = @LastModifierUserId, Code = @Code, Price = @Discount, StartDate = @StartDate, EndDate = @EndDate where Id = @Id
	select * from GetAllPromotion where Id = @Id
go
--proc add Food
create proc CreateAccessoryAndFood
@CreatorUserId uniqueidentifier, @Name nvarchar(30), @Size nvarchar(30), @Price int ,@Type tinyint
as
	insert into AccessoryAndFood(CreationTime, CreatorUserId, IsDeleted, Name,Type, Size, Price) values (getdate(), @CreatorUserId, 0, @Name,@Type, @Size, @Price)
go
--proc update Food
create proc UpdateAccessoryAndFood
@LastModifierUserId uniqueidentifier, @Id uniqueidentifier, @Name nvarchar(30),@Type tinyint , @Size nvarchar(30), @Price int
as
	update AccessoryAndFood set LastModificationTime = getdate(), LastModifierUserId = @LastModifierUserId, Name = @Name, Size = @Size, Price = @Price ,Type = @Type  where Id = @Id
go
create proc Login
@Email nvarchar(50), @Password nvarchar(30)
as
	select * from Account where Email = @Email and Password = @Password
	if(@@rowcount = 1)
		print 'Login success!'
	else
		print 'Login fail -_-'
go
select * from Promotion
insert into Account(CreationTime, IsDeleted, Email, Password, Role, Name, IdentityCard, DoB, Address, Phone, Point) values (getdate(), 0, 'admin@gmail.com', '123456', 2, N'ad văn min', '001122334455', '2001-01-01', N'Hà Nội', '0123456789', 0)
declare @AdminId uniqueidentifier = (select Id from (select top 1 * from Account) a)
INSERT INTO AccessoryAndFood ([CreationTime],[CreatorUserId],[IsDeleted],[Name],Type,[Size],[Price])VALUES('2023/04/10',@AdminId,0,'Serious Mass 12Lbs ',1,'L',1590000)
INSERT INTO AccessoryAndFood ([CreationTime],[CreatorUserId],[IsDeleted],[Name],Type,[Size],[Price])VALUES('2023/04/10',@AdminId,0,'Rule 1 Proteins 5Lbs ',1,'S',1680000)
INSERT INTO AccessoryAndFood ([CreationTime],[CreatorUserId],[IsDeleted],[Name],Type,[Size],[Price])VALUES('2023/04/10',@AdminId,0,'Rule 1 Protein 10Lbs ',1,'M',3590000)
INSERT INTO AccessoryAndFood ([CreationTime],[CreatorUserId],[IsDeleted],[Name],Type,[Size],[Price])VALUES('2023/04/10',@AdminId,0,'Rule 1 Whey Blend 5Lbs ',1,'S',1350000)
GO

select * from AccessoryAndFood


